Continuous Integration README

Two stages: build & test

A LAMMPS version is only executed in the CI if files within the following paths are modified:
  - {VERSION}/
  - sources/{VERSION}/
  - ci/
