#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#define loadretry_csr 0x020
#define stallvpu_csr 0x060
#define renaming_csr 0x080
#define arithinst_csr 0x160
#define meminst_csr 0x180


#define vec_inst_csr 0x040
#define cycvpuacticsr 0x0E0
#include <sys/syscall.h>
#include <unistd.h>


//(void setup_counter(uint16_t csr, uint64_t event){
//    long ret = syscall(__NR_arch_specific_syscall+20, csr, event);
//    printf("SYCALL RETURN = %ld\n", ret);
//}

#define str(a) #a
#define read_counter(reg) \
({ \
        uint64_t counter; \
        asm volatile("csrr %0, " str(reg) : "=r"(counter)  );/* : "r" (value)); */\
       	counter; \
})

void setup_counter(uint16_t csr, uint64_t event);
