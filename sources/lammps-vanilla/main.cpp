/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://www.lammps.org/, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "lammps.h"

#include "accelerator_kokkos.h"
#include "input.h"
#include "lmppython.h"

#if defined(LAMMPS_EXCEPTIONS)
#include "exceptions.h"
#endif

#include <cstdlib>
#include <mpi.h>

#if defined(LAMMPS_TRAP_FPE) && defined(_GNU_SOURCE)
#include <fenv.h>
#endif

// import MolSSI Driver Interface library
#if defined(LMP_MDI)
#include <mdi.h>
#endif

#ifdef USE_EXTRAE
#include "extrae_user_events.h"
#endif

#ifdef USE_CSSR
#define EPI_ARCH
#include "KSPACE/counters.h"
void setup_counter(uint16_t csr, uint64_t event){
    long ret = syscall(__NR_arch_specific_syscall+20, csr, event);
    printf("SYCALL RETURN = %ld\n", ret);
}
#endif

using namespace LAMMPS_NS;

/* ----------------------------------------------------------------------
   main program to drive LAMMPS
------------------------------------------------------------------------- */

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);

  MPI_Comm lammps_comm = MPI_COMM_WORLD;

#ifdef USE_EXTRAE
  extrae_type_t type_l0 = 1000;
  unsigned int nvalues_l0 = 2;
  extrae_value_t values_l0[2] = {0, 1};
  char * description_values_l0[2] = {(char *)"End", (char *)"compute"};

  extrae_type_t type_l1 = 1001;
  unsigned int nvalues_l1 = 3;
  extrae_value_t values_l1[3] = {0, 1, 2};
  char * description_values_l1[3] = {(char *)"End", (char *)"ev_init", (char *)"virial_fdotr_compute"};

  Extrae_init();
  Extrae_define_event_type (&type_l0, (char *)"l0_funcs", &nvalues_l0, values_l0, description_values_l0);
  Extrae_define_event_type (&type_l1, (char *)"l1_funcs", &nvalues_l1, values_l1, description_values_l1);
  Extrae_eventandcounters(1000, 0);
  Extrae_eventandcounters(1001, 0);
#endif

#ifdef USE_CSSR
  setup_counter(0x323, vec_inst_csr);
  setup_counter(0x324, cycvpuacticsr);
  unsigned long starttotcyc = read_counter(cycle);
  unsigned long starttotinstr = read_counter(instret);
  unsigned long starttotvecinstr = read_counter(hpmcounter3);
  unsigned long starttotveccyc = read_counter(hpmcounter4);
#endif

#if defined(LMP_MDI)
  // initialize MDI interface, if compiled in

  int mdi_flag;
  if (MDI_Init(&argc, &argv)) MPI_Abort(MPI_COMM_WORLD, 1);
  if (MDI_Initialized(&mdi_flag)) MPI_Abort(MPI_COMM_WORLD, 1);

  // get the MPI communicator that spans all ranks running LAMMPS
  // when using MDI, this may be a subset of MPI_COMM_WORLD

  if (mdi_flag)
    if (MDI_MPI_get_world_comm(&lammps_comm)) MPI_Abort(MPI_COMM_WORLD, 1);
#endif

#if defined(LAMMPS_TRAP_FPE) && defined(_GNU_SOURCE)
  // enable trapping selected floating point exceptions.
  // this uses GNU extensions and is only tested on Linux
  // therefore we make it depend on -D_GNU_SOURCE, too.
  fesetenv(FE_NOMASK_ENV);
  fedisableexcept(FE_ALL_EXCEPT);
  feenableexcept(FE_DIVBYZERO);
  feenableexcept(FE_INVALID);
  feenableexcept(FE_OVERFLOW);
#endif

#ifdef LAMMPS_EXCEPTIONS
  try {
    auto lammps = new LAMMPS(argc, argv, lammps_comm);
    lammps->input->file();
    delete lammps;
  } catch (LAMMPSAbortException &ae) {
    KokkosLMP::finalize();
    Python::finalize();
    MPI_Abort(ae.universe, 1);
  } catch (LAMMPSException &) {
    KokkosLMP::finalize();
    Python::finalize();
    MPI_Barrier(lammps_comm);
    MPI_Finalize();
    exit(1);
  } catch (fmt::format_error &fe) {
    fprintf(stderr, "fmt::format_error: %s\n", fe.what());
    KokkosLMP::finalize();
    Python::finalize();
    MPI_Abort(MPI_COMM_WORLD, 1);
    exit(1);
  }
#else
  try {
    auto lammps = new LAMMPS(argc, argv, lammps_comm);
    lammps->input->file();
    delete lammps;
  } catch (fmt::format_error &fe) {
    fprintf(stderr, "fmt::format_error: %s\n", fe.what());
    KokkosLMP::finalize();
    Python::finalize();
    MPI_Abort(MPI_COMM_WORLD, 1);
    exit(1);
  }
#endif
  KokkosLMP::finalize();
  Python::finalize();
  MPI_Barrier(lammps_comm);
  MPI_Finalize();

#ifdef USE_CSSR
  unsigned long endtotcyc = read_counter(cycle);
  unsigned long endtotinstr = read_counter(instret);
  unsigned long endtotvecinstr = read_counter(hpmcounter3);
  unsigned long endtotveccyc = read_counter(hpmcounter4);
  unsigned long total_cycles = endtotcyc - starttotcyc;
  unsigned long total_instructions = endtotinstr - starttotinstr;
  unsigned long total_vec_cycles = endtotveccyc - starttotveccyc;
  unsigned long total_vec_instructions = endtotvecinstr - starttotvecinstr;
  printf("scalar_total_cycles %lu scalar_total_instr %lu vector_total_cycles %lu vector_total_instr %lu\n", total_cycles, total_instructions, total_vec_cycles, total_vec_instructions);
#endif

#ifdef USE_EXTRAE
  Extrae_fini();
#endif

}

