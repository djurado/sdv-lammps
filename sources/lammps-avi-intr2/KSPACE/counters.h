#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#ifdef EPI_ARCH
	#define loadretry_csr 0x020
	#define stallvpu_csr 0x060
	#define renaming_csr 0x080
	#define arithinst_csr 0x160
	#define meminst_csr 0x180


	#define vec_inst_csr 0x040
	#define cycvpuacticsr 0x0E0
	#include <sys/syscall.h>
	#include <unistd.h>
	void setup_counter(uint16_t csr, uint64_t event){
	    long ret = syscall(__NR_arch_specific_syscall+20, csr, event);
	    printf("SYCALL RETURN = %ld\n", ret);
	}

	#define str(a) #a
	#define read_counter(reg) \
	({ \
	        uint64_t counter; \
	        asm volatile("csrr %0, " str(reg) : "=r"(counter)  );/* : "r" (value)); */\
        	counter; \
	})
#endif


const uint32_t kNumPMC = 16;

uint64_t *init_hw_counters()
{
#ifdef NEC_ARCH
    return (uint64_t *)malloc(kNumPMC * sizeof(uint64_t));
#elif defined (EPI_ARCH)
    setup_counter(0x323, vec_inst_csr);  //803
    setup_counter(0x324, cycvpuacticsr); //804
    return (uint64_t *)malloc(4 * sizeof(uint64_t));
#endif
}

void hw_counters_start(uint64_t * events)
{
#ifdef NEC_ARCH
    uint64_t pmc00, pmc01, pmc02, pmc03, pmc04, pmc05, pmc06, pmc07, pmc08,
        pmc09, pmc10, pmc11, pmc12, pmc13, pmc14, pmc15, usrcc;

    __asm__ volatile(
		     "smir %0, %%pmc00\n\t"
                     "smir %1, %%pmc01\n\t"
                     "smir %2, %%pmc02\n\t"
                     "smir %3, %%pmc03\n\t"
                     "smir %4, %%pmc04\n\t"
                     "smir %5, %%pmc05\n\t"
                     "smir %6, %%pmc06\n\t"
                     "smir %7, %%pmc07\n\t"
                     "smir %8, %%pmc08\n\t"
                     "smir %9, %%pmc09\n\t"
                     "smir %10, %%pmc10\n\t"
                     "smir %11, %%pmc11\n\t"
                     "smir %12, %%pmc12\n\t"
                     "smir %13, %%pmc13\n\t"
                     "smir %14, %%pmc14\n\t"
                     "smir %15, %%pmc15\n\t"
                     "smir %16, %%usrcc\n\t"
                     : 
		       "=r"(pmc00),
                       "=r"(pmc01),
                       "=r"(pmc02),
                       "=r"(pmc03),
                       "=r"(pmc04),
                       "=r"(pmc05),
                       "=r"(pmc06),
                       "=r"(pmc07),
                       "=r"(pmc08),
                       "=r"(pmc09),
                       "=r"(pmc10),
                       "=r"(pmc11),
                       "=r"(pmc12),
                       "=r"(pmc13),
                       "=r"(pmc14),
                       "=r"(pmc15),
                       "=r"(usrcc));
    events[0] = pmc00;
    events[1] = pmc01;
    events[2] = pmc02;
    events[3] = pmc03;
    events[4] = pmc04;
    events[5] = pmc05;
    events[6] = pmc06;
    events[7] = pmc07;
    events[8] = pmc08;
    events[9] = pmc09;
    events[10] = pmc10;
    events[11] = pmc11;
    events[12] = pmc12;
    events[13] = pmc13;
    events[14] = pmc14;
    events[15] = pmc15;
    events[16] = usrcc;
#elif defined (EPI_ARCH)
	uint64_t c1 = read_counter(hpmcounter3);
	uint64_t c2 = read_counter(hpmcounter4);
	uint64_t c3 = read_counter(instret);
	uint64_t c4 = read_counter(cycle);
	events[0] = c1; events[1] = c2; events[2] = c3; events[3] = c4;
#endif
}

void hw_counters_stop(uint64_t * events)
{

#ifdef NEC_ARCH
    uint64_t pmc00, pmc01, pmc02, pmc03, pmc04, pmc05, pmc06,
        pmc07, pmc08, pmc09, pmc10, pmc11, pmc12, pmc13, pmc14, pmc15, usrcc;

    /*  
        @ MODE 0 VE_PERF_MODE=VECTOR-OP
        pmc00: Execution count
        pmc01: Vector execution count
        pmc02: Floating point data element count
        pmc03: Vector elements count
        pmc04: Vector execution clock count
        pmc05: L1 cache miss clock count
        pmc06: Vector elements count 2
        pmc07: Vector arithmetic execution clock count (VAREC)
        pmc08: Vector load execution clock count (VLDEC)
        pmc09: Port conflict clock count (PCCC)
        pmc10: Vector load delayed clock count (VLDCC)
        pmc11: Vector load element count
        pmc12: Vector load cache miss element count
        pmc13: Fused multiply add element count
        pmc14: Power throttling clock count
        pmc15: Thermal throttling clock count
        usrcc: User Clock Count
    */

    /*  
        @ MODE 1 VE_PERF_MODE=VECTOR-MEM
        # PMC00 - Execution count (EX)
        # PMC01 - Vector execution count (VX)
        # PMC02 - Floating point data element count (FPEC)
        # PMC03 - L1 instruction cache miss count (L1IMC)       
        # PMC04 - L1 instruction cache access count (L1IAC)
        # PMC05 - L1 operand cache miss count (L1OMC)
        # PMC06 - L1 operand cache access count (L1OAC)
        # PMC07 - L2 cache miss count (L2MC)
        # PMC08 - L2 cache access count (L2AC)
        # PMC09 - Branch execution count (BREC)
        # PMC10 - Branch prediction failure count (BPFC)
        # PMC11 - Vector load execution count (VLXC)
        # PMC12 - Vector load cache miss execution count (VLCMX)
        # PMC13 - Fused multiply add execution count (FMAXC)
        # PMC14
        # PMC15
    */

    __asm__ volatile(
                     "smir %16, %%usrcc\n\t"
		     "smir %0, %%pmc00\n\t"
                     "smir %1, %%pmc01\n\t"
                     "smir %2, %%pmc02\n\t"
                     "smir %3, %%pmc03\n\t"
                     "smir %4, %%pmc04\n\t"
                     "smir %5, %%pmc05\n\t"
                     "smir %6, %%pmc06\n\t"
                     "smir %7, %%pmc07\n\t"
                     "smir %8, %%pmc08\n\t"
                     "smir %9, %%pmc09\n\t"
                     "smir %10, %%pmc10\n\t"
                     "smir %11, %%pmc11\n\t"
                     "smir %12, %%pmc12\n\t"
                     "smir %13, %%pmc13\n\t"
                     "smir %14, %%pmc14\n\t"
                     "smir %15, %%pmc15\n\t"
    //                 "smir %16, %%usrcc\n\t"
                     :
		       "=r"(pmc00),
                       "=r"(pmc01),
                       "=r"(pmc02),
                       "=r"(pmc03),
                       "=r"(pmc04),
                       "=r"(pmc05),
                       "=r"(pmc06),
                       "=r"(pmc07),
                       "=r"(pmc08),
                       "=r"(pmc09),
                       "=r"(pmc10),
                       "=r"(pmc11),
                       "=r"(pmc12),
                       "=r"(pmc13),
                       "=r"(pmc14),
                       "=r"(pmc15),
                       "=r"(usrcc));

    events[0] = pmc00 - events[0];
    events[1] = pmc01 - events[1];
    events[2] = pmc02 - events[2];
    events[3] = pmc03 - events[3];
    events[4] = pmc04 - events[4];
    events[5] = pmc05 - events[5];
    events[6] = pmc06 - events[6];
    events[7] = pmc07 - events[7];
    events[8] = pmc08 - events[8];
    events[9] = pmc09 - events[9];
    events[10] = pmc10 - events[10];
    events[11] = pmc11 - events[11];
    events[12] = pmc12 - events[12];
    events[13] = pmc13 - events[13];
    events[14] = pmc14 - events[14];
    events[15] = pmc15 - events[15];
    events[16] = usrcc - events[16];
#elif defined (EPI_ARCH)
	uint64_t c4 = read_counter(cycle);
	uint64_t c3 = read_counter(instret);
	uint64_t c1 = read_counter(hpmcounter3);
	uint64_t c2 = read_counter(hpmcounter4);
	events[0] = c1 - events[0];
	events[1] = c2 - events[1];
	events[2] = c3 - events[2];
	events[3] = c4 - events[3];
#endif
}

void print_hardware_counters(const char *section, const uint64_t *ev)
{
#ifdef NEC_ARCH
    const char *mode = getenv("VE_PERF_MODE");
    if (strcmp(mode,"VECTOR-OP") == 0 || strcmp(mode,"")==0)
    {
        fprintf(stdout, "{\n");
        fprintf(stdout, "    \"VE counters\": { \n");
        fprintf(stdout, "        \"Section\": \"%s\",\n", section);
        fprintf(stdout, "        \"Exec. Inst.\": %" PRIu64 ",\n", ev[0]);
        fprintf(stdout, "        \"Vec. Inst. Execution\": %" PRIu64 ",\n", ev[1]);
        fprintf(stdout, "        \"FP Data Elements\": %" PRIu64 ",\n", ev[2]);
        fprintf(stdout, "        \"Vec. Elements\": %" PRIu64 ",\n", ev[3]);
        fprintf(stdout, "        \"Vec. Exec. Cycles\": %" PRIu64 ",\n", ev[4]);
        fprintf(stdout, "        \"L1$ Miss Cycles\": %" PRIu64 ",\n", ev[5]);
        fprintf(stdout, "        \"Vec. Elements 2\": %" PRIu64 ",\n", ev[6]);
        fprintf(stdout, "        \"Vec. Arithmetic exec. Cycles\": %" PRIu64 ",\n", ev[7]);
        fprintf(stdout, "        \"Vec. Load execution Cycles\": %" PRIu64 ",\n", ev[8]);
        fprintf(stdout, "        \"Port Conflict Cycles\": %" PRIu64 ",\n", ev[9]);
        fprintf(stdout, "        \"Vec. load delayed\": %" PRIu64 ",\n", ev[10]);
        fprintf(stdout, "        \"Vec. Loaded elements\": %" PRIu64 ",\n", ev[11]);
        fprintf(stdout, "        \"Vec. Load Cache Miss Elements\": %" PRIu64 ",\n", ev[12]);
        fprintf(stdout, "        \"FMA Element Count\": %" PRIu64 ",\n", ev[13]);
        // fprintf(stdout, "        \"Power Throttling Cycles\": %"PRIu64",\n", ev[14]);
        // fprintf(stdout, "        \"Thermal Throttling Cycles\": %"PRIu64",\n", ev[15]);
        fprintf(stdout, "        \"User Cycles\": %" PRIu64 "\n", ev[16]);
        fprintf(stdout, "    }\n");
        fprintf(stdout, "}\n");
    }
    else if (strcmp(mode,"VECTOR-MEM") == 0)
    {
        fprintf(stdout, "{\n");
        fprintf(stdout, "    \"VE counters\": { \n");
        fprintf(stdout, "        \"Section\": \"%s\",\n", section);
        fprintf(stdout, "        \"Exec. Inst.\": %" PRIu64 ",\n", ev[0]);
        fprintf(stdout, "        \"Vec. Inst. Execution\": %" PRIu64 ",\n", ev[1]);
        fprintf(stdout, "        \"FP Data Elements\": %" PRIu64 ",\n", ev[2]);
        fprintf(stdout, "        \"L1I$ Miss Count\": %" PRIu64 ",\n", ev[3]);
        fprintf(stdout, "        \"L1I$ Access Count\": %" PRIu64 ",\n", ev[4]);
        fprintf(stdout, "        \"L1D$ Miss Count\": %" PRIu64 ",\n", ev[5]);
        fprintf(stdout, "        \"L1D$ Access Count\": %" PRIu64 ",\n", ev[6]);
        fprintf(stdout, "        \"L2$ Miss Count\": %" PRIu64 ",\n", ev[7]);
        fprintf(stdout, "        \"L2$ Access Count\": %" PRIu64 ",\n", ev[8]);
        fprintf(stdout, "        \"Branch Exec. Count\": %" PRIu64 ",\n", ev[9]);
        fprintf(stdout, "        \"Branch Pred. Fail Count\": %" PRIu64 ",\n", ev[10]);
        fprintf(stdout, "        \"Vec. Load Exec. Count\": %" PRIu64 ",\n", ev[11]);
        fprintf(stdout, "        \"Vec. Load Miss Exec. Count\": %" PRIu64 ",\n", ev[12]);
        fprintf(stdout, "        \"FMA Exec. Count\": %" PRIu64 ",\n", ev[13]);
        //fprintf(stdout, "        \"Power Throttling Cycles\": %"PRIu64",\n", ev[14]);
        // fprintf(stdout, "        \"Thermal Throttling Cycles\": %"PRIu64",\n", ev[15]);
        fprintf(stdout, "        \"User Cycles\": %" PRIu64 "\n", ev[16]);
        fprintf(stdout, "    }\n");
        fprintf(stdout, "}\n");
    }
#elif defined (EPI_ARCH)
        fprintf(stdout, "cnt %s vecinst %ld\n", section, ev[0]);
        fprintf(stdout, "cnt %s vpuactive %ld\n", section, ev[1]);
        fprintf(stdout, "cnt %s totinst %ld\n", section, ev[2]);
        fprintf(stdout, "cnt %s totcyc %ld\n", section, ev[3]);
#endif
}
