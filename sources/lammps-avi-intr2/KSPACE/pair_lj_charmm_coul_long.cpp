// clang-format off
/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://www.lammps.org/, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Paul Crozier (SNL)
------------------------------------------------------------------------- */

#include "pair_lj_charmm_coul_long.h"

#include "atom.h"
#include "comm.h"
#include "error.h"
#include "force.h"
#include "kspace.h"
#include "memory.h"
#include "neigh_list.h"
#include "neighbor.h"
#include "respa.h"
#include "update.h"

#include <cmath>
#include <cstring>

#include <cfloat>

#ifdef USE_CSSR
#define EPI_ARCH
#include "counters.h"
#endif

#ifdef USE_EXTRAE
#include "extrae_user_events.h"
#endif

using namespace LAMMPS_NS;

#define EWALD_F   1.12837917
#define EWALD_P   0.3275911
#define A1        0.254829592
#define A2       -0.284496736
#define A3        1.421413741
#define A4       -1.453152027
#define A5        1.061405429

/* ---------------------------------------------------------------------- */

PairLJCharmmCoulLong::PairLJCharmmCoulLong(LAMMPS *lmp) : Pair(lmp)
{
  respa_enable = 1;
  ewaldflag = pppmflag = 1;
  ftable = nullptr;
  implicit = 0;
  mix_flag = ARITHMETIC;
  writedata = 1;
  cut_respa = nullptr;
}

/* ---------------------------------------------------------------------- */

PairLJCharmmCoulLong::~PairLJCharmmCoulLong()
{
  if (copymode) return;

  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(cutsq);

    memory->destroy(epsilon);
    memory->destroy(sigma);
    memory->destroy(eps14);
    memory->destroy(sigma14);
    memory->destroy(lj1);
    memory->destroy(lj2);
    memory->destroy(lj3);
    memory->destroy(lj4);
    memory->destroy(lj14_1);
    memory->destroy(lj14_2);
    memory->destroy(lj14_3);
    memory->destroy(lj14_4);
    memory->destroy(offset);
  }
  if (ftable) free_tables();
}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_iterj_special(int i, int j)
{
  int itype,jtype, itable;
  double qtmp,delx,dely,delz,rsq,fpair;
  double fraction, table;
  double r,r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double grij,expm2,prefactor,t,erfc;
  double philj,switch1,switch2;

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double qqrd2e = force->qqrd2e;
    
  factor_lj = special_lj[sbmask(j)];
  factor_coul = special_coul[sbmask(j)];
  j &= NEIGHMASK;

  qtmp = q[i];
  delx = x[i][0] - x[j][0];
  dely = x[i][1] - x[j][1];
  delz = x[i][2] - x[j][2];
  itype = type[i];
  rsq = delx*delx + dely*dely + delz*delz;

  if (rsq < cut_bothsq) {

    if (!ncoultablebits || rsq <= tabinnersq) {

      r = sqrt(rsq);
      grij = g_ewald * r;
      expm2 = exp(-grij*grij);
      t = 1.0 / (1.0 + EWALD_P*grij);
      erfc = t * (A1+t*(A2+t*(A3+t*(A4+t*A5)))) * expm2;
      prefactor = qqrd2e * qtmp*q[j]/r;
      forcecoul = prefactor * (erfc + EWALD_F*grij*expm2);
      if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*prefactor;

    } else {

      union_int_float_t rsq_lookup;
      rsq_lookup.f = rsq;
      itable = rsq_lookup.i & ncoulmask;
      itable >>= ncoulshiftbits;

      fraction = (rsq - rtable[itable]) * drtable[itable];
      table = ftable[itable] + fraction*dftable[itable];
      forcecoul = qtmp*q[j] * table;
      if (factor_coul < 1.0) {
        table = ctable[itable] + fraction*dctable[itable];
        prefactor = qtmp*q[j] * table;
        forcecoul -= (1.0-factor_coul)*prefactor;
      }

    }

    r2inv = 1.0/rsq;
    r6inv = r2inv*r2inv*r2inv;
    jtype = type[j];
    forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
    if (rsq > cut_lj_innersq) {
      switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
        (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
      switch2 = 12.0*rsq * (cut_ljsq-rsq) *
        (rsq-cut_lj_innersq) * denom_lj_inv;
      philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
      forcelj = forcelj*switch1 + philj*switch2;
    }

    fpair = (forcecoul + factor_lj*forcelj) * r2inv;

    f[i][0] += delx*fpair;
    f[i][1] += dely*fpair;
    f[i][2] += delz*fpair;
    if (newton_pair || j < nlocal) {
      f[j][0] -= delx*fpair;
      f[j][1] -= dely*fpair;
      f[j][2] -= delz*fpair;
    }
  }
}

/* ---------------------------------------------------------------------- */

typedef union {
  long l;
  double d;
} union_long_double_t;

long times_loopi_special = 0;
FILE *fptr;

//void PairLJCharmmCoulLong::compute_loopj(int i)
void PairLJCharmmCoulLong::compute_loopj(long i)
{
  //int j,jj,jnum,itype,jtype,itable;
  long j,junmasked,jj,jnum,itype,jtype,itable;
  long ncoulmask64, ncoulshiftbits64;
  double qtmp,xtmp,ytmp,ztmp,delx,dely,delz,fpair;
  double fraction,table;
  double r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double prefactor;
  double philj,switch1,switch2;
  int *jlist,*numneigh,**firstneigh;
  double rsq;

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  //int nlocal = atom->nlocal;
  long nlocal = (long) atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  //int newton_pair = force->newton_pair;
  long newton_pair = (long) force->newton_pair;

  long ej;
  //union_int_float_t rsq_lookup;
  union_long_double_t rsq_lookup64;

  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms
    
  qtmp = q[i];
  xtmp = x[i][0];
  ytmp = x[i][1];
  ztmp = x[i][2];
  itype = type[i];
  jlist = firstneigh[i];
  //jnum = numneigh[i];
  jnum = (long) numneigh[i];

  ncoulmask64 = ((long) ncoulmask) << (DBL_MANT_DIG - FLT_MANT_DIG);
  ncoulshiftbits64 = ncoulshiftbits + (DBL_MANT_DIG - FLT_MANT_DIG);

  ej = 0;

  if (jnum == 1 || jnum%2 == 1) {
    compute_iterj_special(i, jlist[jnum - 1]);
  }

  // CONSTANTS
  long halfjnum = jnum / 2;
  long vlmax = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);

  __epi_1xf64 vecfpairdelxred = __builtin_epi_vbroadcast_1xf64(0.0, vlmax);
  __epi_1xf64 vecfpairdelyred = __builtin_epi_vbroadcast_1xf64(0.0, vlmax);
  __epi_1xf64 vecfpairdelzred = __builtin_epi_vbroadcast_1xf64(0.0, vlmax);
  for (jj = 0; jj < halfjnum;) {

    long gvl = __builtin_epi_vsetvl(2*(halfjnum - jj), __epi_e32, __epi_m1);
    // convert jlist[jj] and type[jlist[jj]&NEIGHMASK to 64 bits
    __epi_2xi32 vecjunmasked32 = __builtin_epi_vload_2xi32(&jlist[2*jj], gvl);
    __epi_2xi32 vecj32 = __builtin_epi_vload_2xi32(&jlist[2*jj], gvl);
    vecj32 = __builtin_epi_vand_2xi32(vecj32, __builtin_epi_vbroadcast_2xi32(NEIGHMASK, gvl), gvl);
    // convert sbmask from 32b element index to byte offset
    vecj32 = __builtin_epi_vsll_2xi32(vecj32, __builtin_epi_vbroadcast_2xi32(2, gvl), gvl);
    __epi_2xi32 vecjtype32 = __builtin_epi_vload_indexed_2xi32(type, vecj32, gvl);

   
    // reinterpret vecjunmasked32 and vectype32 (__epi_2xi32) as __epi_1xi64 
    // TODO investigate further why doing two vmv.v.v in the same block  doesnt work with O0
    // it translates into mv a b, mv b a (vecjunmasked64 destroys vecjtype64)
    __epi_1xi64 vecjunmasked64;
    __epi_1xi64 vecjtype64;
    /*__asm__ (
    "nop\n\t"
    "vsetvli %0, %3, e64, m1\n\t"
    "vmv.v.v %1, %4\n\t"
    "vmv.v.v %2, %5\n\t"
    : "=r" (gvl), "=vr" (vecjunmasked64), "=vr" (vecjtype64)
    : "r" (halfjnum - jj), "vr" (vecjunmasked32), "vr" (vecjtype32)
    );*/

    __asm__ (
    "nop\n\t"
    "vsetvli %0, %2, e64, m1\n\t"
    "vmv.v.v %1, %3\n\t"
    : "=r" (gvl), "=vr" (vecjtype64)
    : "r" (halfjnum - jj), "vr" (vecjtype32)
    );

    __asm__ (
    "nop\n\t"
    "vsetvli %0, %2, e64, m1\n\t"
    "vmv.v.v %1, %3\n\t"
    : "=r" (gvl), "=vr" (vecjunmasked64)
    : "r" (halfjnum - jj), "vr" (vecjunmasked32)
    );
    gvl = __builtin_epi_vsetvl(halfjnum - jj, __epi_e64, __epi_m1);
    //vecjunmasked64 = __builtin_epi_vload_1xi64((long *) &jlist[2*jj], gvl);

    for (long idhalf = 0; idhalf < 2 ; ++idhalf) {
      //junmasked = (long) jlist[jj];
      __epi_1xi64 vecjunmasked = __builtin_epi_vand_1xi64(vecjunmasked64, __builtin_epi_vbroadcast_1xi64(0x00000000FFFFFFFF, gvl), gvl);
      __epi_1xi64 vecjtype = __builtin_epi_vand_1xi64(vecjtype64, __builtin_epi_vbroadcast_1xi64(0x00000000FFFFFFFF, gvl), gvl);

      // TODO - do it elsewhere only for masked elements? will it be faster?
      // TODO Try to not use indexed? - arrays only have 4 positions - try to use mask and "merge" approach?
      //factor_lj = special_lj[sbmask(junmasked)];
      //TODO SBBITS = 30. Doing &3 isnt actually needed if the shift is logic
      __epi_1xi64 vecjsbmask = __builtin_epi_vsrl_1xi64(vecjunmasked, __builtin_epi_vbroadcast_1xi64(SBBITS, gvl), gvl);
      vecjsbmask = __builtin_epi_vand_1xi64(vecjsbmask, __builtin_epi_vbroadcast_1xi64(3, gvl), gvl);
      // convert sbmask from 64b element index to byte offset
      vecjsbmask = __builtin_epi_vsll_1xi64(vecjsbmask, __builtin_epi_vbroadcast_1xi64(3, gvl), gvl);
      __epi_1xf64 vecfactor_lj = __builtin_epi_vload_indexed_1xf64(special_lj, vecjsbmask, gvl);
      //factor_coul = special_coul[sbmask(j)];
      __epi_1xf64 vecfactor_coul = __builtin_epi_vload_indexed_1xf64(special_coul, vecjsbmask, gvl);

      //j &= NEIGHMASK; 
      __epi_1xi64 vecj = __builtin_epi_vand_1xi64(vecjunmasked, __builtin_epi_vbroadcast_1xi64(NEIGHMASK, gvl), gvl);
     
      // this is a pointer to pointer (not a multidimensional array)
      //delx = xtmp - x[j][0];
      //dely = ytmp - x[j][1];
      //delz = ytmp - x[j][2];
      // convert sbmask from 64b element index to byte offset
      __epi_1xi64 vecjbytes = __builtin_epi_vsll_1xi64(vecj, __builtin_epi_vbroadcast_1xi64(3, gvl), gvl);
      // vecxj -> x[j] -> double *
      __epi_1xi64 vecxj = __builtin_epi_vload_indexed_1xi64((long *) x, vecjbytes, gvl);
      // access x[j][0], x[j][1], x[j][2]
      __epi_1xf64 vecdelx = __builtin_epi_vload_indexed_1xf64((double *) 0, vecxj, gvl);
      vecdelx = __builtin_epi_vfsub_1xf64(__builtin_epi_vbroadcast_1xf64(xtmp, gvl), vecdelx, gvl);
      __epi_1xf64 vecdely = __builtin_epi_vload_indexed_1xf64((double *) 8, vecxj, gvl);
      vecdely = __builtin_epi_vfsub_1xf64(__builtin_epi_vbroadcast_1xf64(ytmp, gvl), vecdely, gvl);
      __epi_1xf64 vecdelz = __builtin_epi_vload_indexed_1xf64((double *) 16, vecxj, gvl);
      vecdelz = __builtin_epi_vfsub_1xf64(__builtin_epi_vbroadcast_1xf64(ztmp, gvl), vecdelz, gvl);

      //rsq = delx*delx + dely*dely + delz*delz;
      __epi_1xf64 vecrsq = __builtin_epi_vfmul_1xf64(vecdelx, vecdelx, gvl);
      vecrsq = __builtin_epi_vfmacc_1xf64(vecrsq, vecdely, vecdely, gvl);
      vecrsq = __builtin_epi_vfmacc_1xf64(vecrsq, vecdelz, vecdelz, gvl);

      //if (!ncoultablebits || rsq <= tabinnersq)
      //if !ncoultablebits -> no ncoultable, no point in using vector version
      // ncoultablebits apparently doesn't change during the execution -> check moved outside
      // compute_iterj_special(i, junmasked);
      // tabinnersq should be < bothsq
      __epi_1xi1 vmasktabinnersq = __builtin_epi_vmflt_1xf64(vecrsq, __builtin_epi_vbroadcast_1xf64(tabinnersq, gvl), gvl);
      __epi_1xi1 vmasktabinnersqorig = vmasktabinnersq;
      __epi_1xi64 vecvid = __builtin_epi_vid_1xi64(gvl);
      long first; 
      while ((first = __builtin_epi_vmfirst_1xi1(vmasktabinnersq, gvl)) != -1) {
        // I guess it would be possible to get the index of the element with
        // something like 2*jj + first*2 + idhalf
        compute_iterj_special(i, __builtin_epi_vextract_1xi64(vecjunmasked, first));
        __epi_1xi1 vmasktmp = __builtin_epi_vmseq_1xi64(vecvid, __builtin_epi_vbroadcast_1xi64(first, gvl), gvl);
        vmasktabinnersq = __builtin_epi_vmxor_1xi1(vmasktabinnersq, vmasktmp, gvl);
      }

      // the mask for the elements to be processed vectorially
      __epi_1xi1 vmask = __builtin_epi_vmflt_1xf64(vecrsq, __builtin_epi_vbroadcast_1xf64(cut_bothsq, gvl), gvl);
      vmask = __builtin_epi_vmxor_1xi1(vmask, vmasktabinnersqorig, gvl);
      // TODO mask everything or only mask important things like stores?
      // right now - only mask important things
      //r2inv = 1.0/rsq;
      __epi_1xf64 vecr2inv = __builtin_epi_vfdiv_1xf64(__builtin_epi_vbroadcast_1xf64(1, gvl), vecrsq, gvl);

      //rsq_lookup64.d = rsq;
      //itable = rsq_lookup64.l & ncoulmask64;
      //itable >>= ncoulshiftbits64;
      // I trust that this won't generate OoB accesses for "masked" elements
      // how to interpret a float64 vector as int64? -> inline asm + vmv
      __epi_1xi64 vecitable;
      __asm__ (
      "nop\n\t"
      "vmv.v.v %0, %1\n\t"
      : "=vr" (vecitable)
      : "vr" (vecrsq)
      );
      vecitable = __builtin_epi_vand_1xi64(vecitable, __builtin_epi_vbroadcast_1xi64(ncoulmask64, gvl), gvl);
      vecitable = __builtin_epi_vsrl_1xi64(vecitable, __builtin_epi_vbroadcast_1xi64(ncoulshiftbits64, gvl), gvl);
      // convert sbmask from 64b element index to byte offset
      vecitable = __builtin_epi_vsll_1xi64(vecitable, __builtin_epi_vbroadcast_1xi64(3, gvl), gvl);
     
      __epi_1xf64 vecrtable = __builtin_epi_vload_indexed_1xf64(rtable, vecitable, gvl);
      __epi_1xf64 vecdrtable = __builtin_epi_vload_indexed_1xf64(drtable, vecitable, gvl);
      __epi_1xf64 vecftable = __builtin_epi_vload_indexed_1xf64(ftable, vecitable, gvl);
      __epi_1xf64 vecdftable = __builtin_epi_vload_indexed_1xf64(dftable, vecitable, gvl);
      __epi_1xf64 vecctable = __builtin_epi_vload_indexed_1xf64(ctable, vecitable, gvl);
      __epi_1xf64 vecdctable = __builtin_epi_vload_indexed_1xf64(dctable, vecitable, gvl);
      //fraction = (rsq - rtable[itable]) * drtable[itable];
      __epi_1xf64 vecfraction = __builtin_epi_vfsub_1xf64(vecrsq, vecrtable, gvl);
      vecfraction = __builtin_epi_vfmul_1xf64(vecfraction, vecdrtable, gvl);

      //table = ftable[itable] + fraction*dftable[itable];
      //overwrite ftable
      __epi_1xf64 vectable = __builtin_epi_vfmacc_1xf64(vecftable, vecfraction, vecdftable, gvl);

      //forcecoul = qtmp*q[j] * table;
      __epi_1xf64 vecqtmpqj = __builtin_epi_vload_indexed_1xf64(q, vecjbytes, gvl);
      vecqtmpqj = __builtin_epi_vfmul_1xf64(__builtin_epi_vbroadcast_1xf64(qtmp, gvl), vecqtmpqj, gvl);
      __epi_1xf64 vecforcecoul = __builtin_epi_vfmul_1xf64(vecqtmpqj, vectable, gvl);

      //if (factor_coul < 1.0)
      __epi_1xi1 vmaskfactor_coul = __builtin_epi_vmflt_1xf64(vecfactor_coul, __builtin_epi_vbroadcast_1xf64(1.0, gvl), gvl);
      //table = ctable[itable] + fraction*dctable[itable];
      //prefactor = qtmp*q[j] * table;
      //forcecoul -= (1.0-factor_coul)*prefactor;
      vectable = __builtin_epi_vfmacc_1xf64(vecctable, vecfraction, vecdctable, gvl);     
      __epi_1xf64 vecprefactor = __builtin_epi_vfmul_1xf64(vecqtmpqj, vectable, gvl);
      __epi_1xf64 vecforcecoultmp = __builtin_epi_vfsub_1xf64(__builtin_epi_vbroadcast_1xf64(1.0, gvl), vecfactor_coul, gvl);
      // use masked vfnmsac(c,a,b,mask) -(a*b)+c else c 
      vecforcecoul = __builtin_epi_vfnmsac_1xf64_mask(vecforcecoul, vecforcecoultmp, vecprefactor, vmaskfactor_coul, gvl);
      // endif

      //r6inv = r2inv*r2inv*r2inv;
      __epi_1xf64 vecr6inv = __builtin_epi_vfmul_1xf64(vecr2inv, vecr2inv, gvl);
      vecr6inv = __builtin_epi_vfmul_1xf64(vecr6inv, vecr2inv, gvl);

      //jtype = (long) type[j];
      // computed alongside vecjunmasked
      // again, lj1, lj2, lj3 and lj4 are double **. ljx[itype] as base address, jtype as index
      // convert sbmask from 64b element index to byte offset
      vecjtype = __builtin_epi_vsll_1xi64(vecjtype, __builtin_epi_vbroadcast_1xi64(3, gvl), gvl);
      __epi_1xf64 veclj1 = __builtin_epi_vload_indexed_1xf64(lj1[itype], vecjtype, gvl);
      __epi_1xf64 veclj2 = __builtin_epi_vload_indexed_1xf64(lj2[itype], vecjtype, gvl);
      __epi_1xf64 veclj3 = __builtin_epi_vload_indexed_1xf64(lj3[itype], vecjtype, gvl);
      __epi_1xf64 veclj4 = __builtin_epi_vload_indexed_1xf64(lj4[itype], vecjtype, gvl);
      //forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
      // use vfmsub(a,b,c) a*b - c
      __epi_1xf64 vecforcelj = __builtin_epi_vfmsub_1xf64(veclj1, vecr6inv, veclj2, gvl);
      vecforcelj = __builtin_epi_vfmul_1xf64(vecforcelj, vecr6inv, gvl);

      //if (rsq > cut_lj_innersq) {
      __epi_1xi1 vmaskcut_lj_innersq = __builtin_epi_vmfgt_1xf64(vecrsq, __builtin_epi_vbroadcast_1xf64(cut_lj_innersq, gvl), gvl);
      //switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
      //  (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
      // cut_ljsq -3.0cut_lj_innersq is scalar - can do everything with a single vfmacc
      // TODO you can avoid the last * denom_lj_inv if you integrate it in the other constants - unsafe (and ugly) math?
      __epi_1xf64 vecswitch1 = __builtin_epi_vfmacc_1xf64(__builtin_epi_vbroadcast_1xf64(cut_ljsq - 3.0*cut_lj_innersq, gvl), __builtin_epi_vbroadcast_1xf64(2.0, gvl), vecrsq, gvl);
      __epi_1xf64 vecljsqminusrsq = __builtin_epi_vfsub_1xf64(__builtin_epi_vbroadcast_1xf64(cut_ljsq, gvl), vecrsq, gvl);
      vecswitch1 = __builtin_epi_vfmul_1xf64(vecswitch1, vecljsqminusrsq, gvl);
      vecswitch1 = __builtin_epi_vfmul_1xf64(vecswitch1, vecljsqminusrsq, gvl);
      vecswitch1 = __builtin_epi_vfmul_1xf64(vecswitch1, __builtin_epi_vbroadcast_1xf64(denom_lj_inv, gvl), gvl);

      //switch2 = 12.0*rsq * (cut_ljsq-rsq) *
      //  (rsq-cut_lj_innersq) * denom_lj_inv;
      __epi_1xf64 vecswitch2 = __builtin_epi_vfsub_1xf64(vecrsq, __builtin_epi_vbroadcast_1xf64(cut_lj_innersq, gvl), gvl);
      vecswitch2 = __builtin_epi_vfmul_1xf64(vecswitch2, vecljsqminusrsq, gvl);
      vecswitch2 = __builtin_epi_vfmul_1xf64(vecswitch2, vecrsq, gvl);
      vecswitch2 = __builtin_epi_vfmul_1xf64(vecswitch2, __builtin_epi_vbroadcast_1xf64(12.0 * denom_lj_inv, gvl), gvl);

      //philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
      //vfmsub(a,b,c) = a*b-c
      __epi_1xf64 vecphilj = __builtin_epi_vfmsub_1xf64(veclj3, vecr6inv, veclj4, gvl);
      vecphilj = __builtin_epi_vfmul_1xf64(vecphilj, vecr6inv, gvl);

      //forcelj = forcelj*switch1 + philj*switch2;
      __epi_1xf64 vecforceljtmp = __builtin_epi_vfmul_1xf64(vecphilj, vecswitch2, gvl);
      // masked madd(a,b,c,mask) a*b+c else a
      vecforcelj = __builtin_epi_vfmadd_1xf64_mask(vecforcelj, vecswitch1, vecforceljtmp, vmaskcut_lj_innersq, gvl);
      //endif

      //fpair = (forcecoul + factor_lj*forcelj) * r2inv;
      __epi_1xf64 vecfpair = __builtin_epi_vfmacc_1xf64(vecforcecoul, vecfactor_lj, vecforcelj, gvl);
      vecfpair = __builtin_epi_vfmul_1xf64(vecfpair, vecr2inv, gvl);
      
      //f[i][0] += delx*fpair;
      //f[i][1] += dely*fpair;
      //f[i][2] += delz*fpair;
      // f is **double
      // f[i][0] is reduction
      // TODO do better reduction with just only one vred outside the loop
      //   long vlmax = __builtin_epi_vsetvlmax(__epi_e64, __epi_m1);
      // masked reduction merge, a, b(result), mask
      __epi_1xf64 vecfpairdelx = __builtin_epi_vfmul_1xf64(vecdelx, vecfpair, gvl);
      __epi_1xf64 vecfpairdely = __builtin_epi_vfmul_1xf64(vecdely, vecfpair, gvl);
      __epi_1xf64 vecfpairdelz = __builtin_epi_vfmul_1xf64(vecdelz, vecfpair, gvl);
      //__epi_1xf64 vecfpairdelxred = __builtin_epi_vfredsum_1xf64_mask(__builtin_epi_vbroadcast_1xf64(0.0, gvl), vecfpairdelx, __builtin_epi_vbroadcast_1xf64(0.0, gvl), vmask, gvl);
      //__epi_1xf64 vecfpairdelyred = __builtin_epi_vfredsum_1xf64_mask(__builtin_epi_vbroadcast_1xf64(0.0, gvl), vecfpairdely, __builtin_epi_vbroadcast_1xf64(0.0, gvl), vmask, gvl);
      //__epi_1xf64 vecfpairdelzred = __builtin_epi_vfredsum_1xf64_mask(__builtin_epi_vbroadcast_1xf64(0.0, gvl), vecfpairdelz, __builtin_epi_vbroadcast_1xf64(0.0, gvl), vmask, gvl);
      //f[i][0] += __builtin_epi_vgetfirst_1xf64(vecfpairdelxred);
      //f[i][1] += __builtin_epi_vgetfirst_1xf64(vecfpairdelyred);
      //f[i][2] += __builtin_epi_vgetfirst_1xf64(vecfpairdelzred);
     
      // using vlmax to take advantage of extending missing element with zeros
      // do reduction outside of loop
      //vecfpairdelxred = __builtin_epi_vfadd_1xf64(vecfpairdelxred, vecfpairdelx, vlmax);
      //vecfpairdelyred = __builtin_epi_vfadd_1xf64(vecfpairdelyred, vecfpairdely, vlmax);
      //vecfpairdelzred = __builtin_epi_vfadd_1xf64(vecfpairdelzred, vecfpairdelz, vlmax);
      vecfpairdelxred = __builtin_epi_vfadd_1xf64_mask(vecfpairdelxred, vecfpairdelxred, vecfpairdelx, vmask, vlmax);
      vecfpairdelyred = __builtin_epi_vfadd_1xf64_mask(vecfpairdelyred, vecfpairdelyred, vecfpairdely, vmask, vlmax);
      vecfpairdelzred = __builtin_epi_vfadd_1xf64_mask(vecfpairdelzred, vecfpairdelzred, vecfpairdelz, vmask, vlmax);

      // TODO maybe this can be improved j < nlocal implies ghost atoms?
      // j's in jlist can be not order, something along the lines min > nlocal?
      
      //if (newton_pair || j < nlocal) {
      //  f[j][0] -= delx*fpair;
      //  f[j][1] -= dely*fpair;
      //  f[j][2] -= delz*fpair;
      //  f is double ** - same as x - pointer to pointer
      __epi_1xi1 vmaskjnlocal = __builtin_epi_vmslt_1xi64(vecj, __builtin_epi_vbroadcast_1xi64(nlocal, gvl), gvl);
      vmaskjnlocal = __builtin_epi_vmor_1xi1(vmaskjnlocal, __builtin_epi_cast_1xi1_1xi64(__builtin_epi_vbroadcast_1xi64(newton_pair, gvl)), gvl);
      vmaskjnlocal = __builtin_epi_vmand_1xi1(vmaskjnlocal, vmask, gvl);
      __epi_1xi64 vecfj = __builtin_epi_vload_indexed_1xi64((long *) f, vecjbytes, gvl);
      __epi_1xf64 vecfjx = __builtin_epi_vload_indexed_1xf64((double *) 0, vecfj, gvl);
      __epi_1xf64 vecfjy = __builtin_epi_vload_indexed_1xf64((double *) 8, vecfj, gvl);
      __epi_1xf64 vecfjz = __builtin_epi_vload_indexed_1xf64((double *) 16, vecfj, gvl);
      vecfjx = __builtin_epi_vfsub_1xf64(vecfjx, vecfpairdelx, gvl);
      vecfjy = __builtin_epi_vfsub_1xf64(vecfjy, vecfpairdely, gvl);
      vecfjz = __builtin_epi_vfsub_1xf64(vecfjz, vecfpairdelz, gvl);
      __builtin_epi_vstore_indexed_1xf64_mask((double *) 16, vecfjz, vecfj, vmaskjnlocal, gvl);
      __builtin_epi_vstore_indexed_1xf64_mask((double *) 8, vecfjy, vecfj, vmaskjnlocal, gvl);
      __builtin_epi_vstore_indexed_1xf64_mask((double *) 0, vecfjx, vecfj, vmaskjnlocal, gvl);
     
      if (idhalf == 0) {
        vecjunmasked64 = __builtin_epi_vsrl_1xi64(vecjunmasked64, __builtin_epi_vbroadcast_1xi64(32, gvl), gvl);
        vecjtype64 = __builtin_epi_vsrl_1xi64(vecjtype64, __builtin_epi_vbroadcast_1xi64(32, gvl), gvl);
      }
    }

    jj += gvl;

  }

  // if we have run the vector loop
  if (halfjnum > 0) {
    vecfpairdelxred = __builtin_epi_vfredsum_1xf64(vecfpairdelxred, __builtin_epi_vbroadcast_1xf64(0.0, vlmax), vlmax);
    vecfpairdelyred = __builtin_epi_vfredsum_1xf64(vecfpairdelyred, __builtin_epi_vbroadcast_1xf64(0.0, vlmax), vlmax);
    vecfpairdelzred = __builtin_epi_vfredsum_1xf64(vecfpairdelzred, __builtin_epi_vbroadcast_1xf64(0.0, vlmax), vlmax);
    f[i][0] += __builtin_epi_vgetfirst_1xf64(vecfpairdelxred);
    f[i][1] += __builtin_epi_vgetfirst_1xf64(vecfpairdelyred);
    f[i][2] += __builtin_epi_vgetfirst_1xf64(vecfpairdelzred);
  }

  /*char myfilename[20];
  sprintf(myfilename, "%ld-%ld.txt", times_loopi_special, i);
  FILE *fptr = fopen(myfilename, "w");
  // PRINT ALL THE THINGS
  for (jj = 0; jj < jnum; jj++) {
    j = jlist[jj] & NEIGHMASK;
    fprintf(fptr,"f[%d] %.8e %.8e %.8e\n", j, f[j][0], f[j][1], f[j][2]);
  }
  fclose(fptr);*/

}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_loopi_special()
{
  int i,ii,inum;
  int *ilist;

  ++times_loopi_special;

  inum = list->inum;
  ilist = list->ilist;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    compute_loopj((long) i);
  }

}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_loopi_original(int eflag)
{
  int i,j,ii,jj,inum,jnum,itype,jtype,itable;
  double qtmp,xtmp,ytmp,ztmp,delx,dely,delz,evdwl,ecoul,fpair;
  double fraction,table;
  double r,r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double grij,expm2,prefactor,t,erfc;
  double philj,switch1,switch2;
  int *ilist,*jlist,*numneigh,**firstneigh;
  double rsq;

  evdwl = ecoul = 0.0;

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double qqrd2e = force->qqrd2e;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    qtmp = q[i];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];
    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      factor_coul = special_coul[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq < cut_bothsq) {
        r2inv = 1.0/rsq;

        if (rsq < cut_coulsq) {
          if (!ncoultablebits || rsq <= tabinnersq) {
            r = sqrt(rsq);
            grij = g_ewald * r;
            expm2 = exp(-grij*grij);
            t = 1.0 / (1.0 + EWALD_P*grij);
            erfc = t * (A1+t*(A2+t*(A3+t*(A4+t*A5)))) * expm2;
            prefactor = qqrd2e * qtmp*q[j]/r;
            forcecoul = prefactor * (erfc + EWALD_F*grij*expm2);
            if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*prefactor;
          } else {
            union_int_float_t rsq_lookup;
            rsq_lookup.f = rsq;
            itable = rsq_lookup.i & ncoulmask;
            itable >>= ncoulshiftbits;
            fraction = (rsq_lookup.f - rtable[itable]) * drtable[itable];
            table = ftable[itable] + fraction*dftable[itable];
            forcecoul = qtmp*q[j] * table;
            if (factor_coul < 1.0) {
              table = ctable[itable] + fraction*dctable[itable];
              prefactor = qtmp*q[j] * table;
              forcecoul -= (1.0-factor_coul)*prefactor;
            }
          }
        } else forcecoul = 0.0;

        if (rsq < cut_ljsq) {
          r6inv = r2inv*r2inv*r2inv;
          jtype = type[j];
          forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
          if (rsq > cut_lj_innersq) {
            switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
              (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
            switch2 = 12.0*rsq * (cut_ljsq-rsq) *
              (rsq-cut_lj_innersq) * denom_lj_inv;
            philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
            forcelj = forcelj*switch1 + philj*switch2;
          }
        } else forcelj = 0.0;

        fpair = (forcecoul + factor_lj*forcelj) * r2inv;

        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }

        if (eflag) {
          if (rsq < cut_coulsq) {
            if (!ncoultablebits || rsq <= tabinnersq)
              ecoul = prefactor*erfc;
            else {
              table = etable[itable] + fraction*detable[itable];
              ecoul = qtmp*q[j] * table;
            }
            if (factor_coul < 1.0) ecoul -= (1.0-factor_coul)*prefactor;
          } else ecoul = 0.0;

          if (rsq < cut_ljsq) {
            evdwl = r6inv*(lj3[itype][jtype]*r6inv-lj4[itype][jtype]);
            if (rsq > cut_lj_innersq) {
              switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
                (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
              evdwl *= switch1;
            }
            evdwl *= factor_lj;
          } else evdwl = 0.0;
        }

        if (evflag) ev_tally(i,j,nlocal,newton_pair,
                             evdwl,ecoul,fpair,delx,dely,delz);
      }
    }
  }

}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute(int eflag, int vflag)
{
#ifdef USE_CSSR
  long startcyc = read_counter(cycle);
#endif

#ifdef USE_EXTRAE
  Extrae_eventandcounters(1000, 1);
#endif

  ev_init(eflag,vflag);

  if (!eflag && !evflag && cut_ljsq == cut_bothsq && cut_coulsq == cut_bothsq && ncoultablebits) {
    compute_loopi_special();
  } else {
    compute_loopi_original(eflag);
  }

  if (vflag_fdotr) virial_fdotr_compute();
  
#ifdef USE_CSSR
  long endcyc = read_counter(cycle);
  long cycles = endcyc - startcyc;
  printf("totcyc %ld\n", cycles);
#endif

#ifdef USE_EXTRAE
  Extrae_eventandcounters(1000, 0);
#endif
}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_inner()
{
  int i,j,ii,jj,inum,jnum,itype,jtype;
  double qtmp,xtmp,ytmp,ztmp,delx,dely,delz,fpair;
  double rsq,r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double rsw;
  int *ilist,*jlist,*numneigh,**firstneigh;

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double qqrd2e = force->qqrd2e;

  inum = list->inum_inner;
  ilist = list->ilist_inner;
  numneigh = list->numneigh_inner;
  firstneigh = list->firstneigh_inner;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    qtmp = q[i];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];
    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      factor_coul = special_coul[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq < cut_out_off_sq) {
        r2inv = 1.0/rsq;
        forcecoul = qqrd2e * qtmp*q[j]*sqrt(r2inv);
        if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*forcecoul;

        r6inv = r2inv*r2inv*r2inv;
        jtype = type[j];
        forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);

        fpair = (forcecoul + factor_lj*forcelj) * r2inv;

        if (rsq > cut_out_on_sq) {
          rsw = (sqrt(rsq) - cut_out_on)*cut_out_diff_inv;
          fpair *= 1.0 + rsw*rsw*(2.0*rsw-3.0);
        }

        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }
      }
    }
  }
}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_middle()
{
  int i,j,ii,jj,inum,jnum,itype,jtype;
  double qtmp,xtmp,ytmp,ztmp,delx,dely,delz,fpair;
  double rsq,r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double philj,switch1,switch2;
  double rsw;
  int *ilist,*jlist,*numneigh,**firstneigh;

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double qqrd2e = force->qqrd2e;

  inum = list->inum_middle;
  ilist = list->ilist_middle;
  numneigh = list->numneigh_middle;
  firstneigh = list->firstneigh_middle;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    qtmp = q[i];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];

    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      factor_coul = special_coul[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq < cut_out_off_sq && rsq > cut_in_off_sq) {
        r2inv = 1.0/rsq;
        forcecoul = qqrd2e * qtmp*q[j]*sqrt(r2inv);
        if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*forcecoul;

        r6inv = r2inv*r2inv*r2inv;
        jtype = type[j];
        forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
        if (rsq > cut_lj_innersq) {
          switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
            (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
          switch2 = 12.0*rsq * (cut_ljsq-rsq) *
            (rsq-cut_lj_innersq) * denom_lj_inv;
          philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
          forcelj = forcelj*switch1 + philj*switch2;
        }

        fpair = (forcecoul + factor_lj*forcelj) * r2inv;
        if (rsq < cut_in_on_sq) {
          rsw = (sqrt(rsq) - cut_in_off)*cut_in_diff_inv;
          fpair *= rsw*rsw*(3.0 - 2.0*rsw);
        }
        if (rsq > cut_out_on_sq) {
          rsw = (sqrt(rsq) - cut_out_on)*cut_out_diff_inv;
          fpair *= 1.0 + rsw*rsw*(2.0*rsw - 3.0);
        }

        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }
      }
    }
  }
}

/* ---------------------------------------------------------------------- */

void PairLJCharmmCoulLong::compute_outer(int eflag, int vflag)
{
  int i,j,ii,jj,inum,jnum,itype,jtype,itable;
  double qtmp,xtmp,ytmp,ztmp,delx,dely,delz,evdwl,ecoul,fpair;
  double fraction,table;
  double r,r2inv,r6inv,forcecoul,forcelj,factor_coul,factor_lj;
  double grij,expm2,prefactor,t,erfc;
  double philj,switch1,switch2;
  double rsw;
  int *ilist,*jlist,*numneigh,**firstneigh;
  double rsq;

  evdwl = ecoul = 0.0;
  ev_init(eflag,vflag);

  double **x = atom->x;
  double **f = atom->f;
  double *q = atom->q;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_coul = force->special_coul;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double qqrd2e = force->qqrd2e;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    qtmp = q[i];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];
    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      factor_coul = special_coul[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;
      jtype = type[j];

      if (rsq < cut_bothsq) {
        r2inv = 1.0/rsq;

        if (rsq < cut_coulsq) {
          if (!ncoultablebits || rsq <= tabinnersq) {
            r = sqrt(rsq);
            grij = g_ewald * r;
            expm2 = exp(-grij*grij);
            t = 1.0 / (1.0 + EWALD_P*grij);
            erfc = t * (A1+t*(A2+t*(A3+t*(A4+t*A5)))) * expm2;
            prefactor = qqrd2e * qtmp*q[j]/r;
            forcecoul = prefactor * (erfc + EWALD_F*grij*expm2 - 1.0);
            if (rsq > cut_in_off_sq) {
              if (rsq < cut_in_on_sq) {
                rsw = (r - cut_in_off)/cut_in_diff;
                forcecoul += prefactor*rsw*rsw*(3.0 - 2.0*rsw);
                if (factor_coul < 1.0)
                  forcecoul -=
                    (1.0-factor_coul)*prefactor*rsw*rsw*(3.0 - 2.0*rsw);
              } else {
                forcecoul += prefactor;
                if (factor_coul < 1.0)
                  forcecoul -= (1.0-factor_coul)*prefactor;
              }
            }
          } else {
            union_int_float_t rsq_lookup;
            rsq_lookup.f = rsq;
            itable = rsq_lookup.i & ncoulmask;
            itable >>= ncoulshiftbits;
            fraction = (rsq_lookup.f - rtable[itable]) * drtable[itable];
            table = ftable[itable] + fraction*dftable[itable];
            forcecoul = qtmp*q[j] * table;
            if (factor_coul < 1.0) {
              table = ctable[itable] + fraction*dctable[itable];
              prefactor = qtmp*q[j] * table;
              forcecoul -= (1.0-factor_coul)*prefactor;
            }
          }
        } else forcecoul = 0.0;

        if (rsq < cut_ljsq && rsq > cut_in_off_sq) {
          r6inv = r2inv*r2inv*r2inv;
          forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
          if (rsq > cut_lj_innersq) {
            switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
              (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
            switch2 = 12.0*rsq * (cut_ljsq-rsq) *
              (rsq-cut_lj_innersq) * denom_lj_inv;
            philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
            forcelj = forcelj*switch1 + philj*switch2;
          }
          if (rsq < cut_in_on_sq) {
            rsw = (sqrt(rsq) - cut_in_off)/cut_in_diff;
            forcelj *= rsw*rsw*(3.0 - 2.0*rsw);
          }
        } else forcelj = 0.0;

        fpair = (forcecoul + forcelj) * r2inv;

        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }

        if (eflag) {
          if (rsq < cut_coulsq) {
            if (!ncoultablebits || rsq <= tabinnersq) {
              ecoul = prefactor*erfc;
              if (factor_coul < 1.0) ecoul -= (1.0-factor_coul)*prefactor;
            } else {
              table = etable[itable] + fraction*detable[itable];
              ecoul = qtmp*q[j] * table;
              if (factor_coul < 1.0) {
                table = ptable[itable] + fraction*dptable[itable];
                prefactor = qtmp*q[j] * table;
                ecoul -= (1.0-factor_coul)*prefactor;
              }
            }
          } else ecoul = 0.0;

          if (rsq < cut_ljsq) {
            r6inv = r2inv*r2inv*r2inv;
            evdwl = r6inv*(lj3[itype][jtype]*r6inv-lj4[itype][jtype]);
            if (rsq > cut_lj_innersq) {
              switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
                (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
              evdwl *= switch1;
            }
            evdwl *= factor_lj;
          } else evdwl = 0.0;
        }

        if (vflag) {
          if (rsq < cut_coulsq) {
            if (!ncoultablebits || rsq <= tabinnersq) {
              forcecoul = prefactor * (erfc + EWALD_F*grij*expm2);
              if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*prefactor;
            } else {
              table = vtable[itable] + fraction*dvtable[itable];
              forcecoul = qtmp*q[j] * table;
              if (factor_coul < 1.0) {
                table = ptable[itable] + fraction*dptable[itable];
                prefactor = qtmp*q[j] * table;
                forcecoul -= (1.0-factor_coul)*prefactor;
              }
            }
          } else forcecoul = 0.0;

          if (rsq <= cut_in_off_sq) {
            r6inv = r2inv*r2inv*r2inv;
            forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
            if (rsq > cut_lj_innersq) {
              switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
                (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
              switch2 = 12.0*rsq * (cut_ljsq-rsq) *
                (rsq-cut_lj_innersq) * denom_lj_inv;
              philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
              forcelj = forcelj*switch1 + philj*switch2;
            }
          } else if (rsq <= cut_in_on_sq) {
            r6inv = r2inv*r2inv*r2inv;
            forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
            if (rsq > cut_lj_innersq) {
              switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
                (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
              switch2 = 12.0*rsq * (cut_ljsq-rsq) *
                (rsq-cut_lj_innersq) * denom_lj_inv;
              philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
              forcelj = forcelj*switch1 + philj*switch2;
            }
          }

          fpair = (forcecoul + factor_lj*forcelj) * r2inv;
        }

        if (evflag) ev_tally(i,j,nlocal,newton_pair,
                             evdwl,ecoul,fpair,delx,dely,delz);
      }
    }
  }
}

/* ----------------------------------------------------------------------
   allocate all arrays
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::allocate()
{
  allocated = 1;
  int n = atom->ntypes;

  memory->create(setflag,n+1,n+1,"pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      setflag[i][j] = 0;

  memory->create(cutsq,n+1,n+1,"pair:cutsq");

  memory->create(epsilon,n+1,n+1,"pair:epsilon");
  memory->create(sigma,n+1,n+1,"pair:sigma");
  memory->create(eps14,n+1,n+1,"pair:eps14");
  memory->create(sigma14,n+1,n+1,"pair:sigma14");
  memory->create(lj1,n+1,n+1,"pair:lj1");
  memory->create(lj2,n+1,n+1,"pair:lj2");
  memory->create(lj3,n+1,n+1,"pair:lj3");
  memory->create(lj4,n+1,n+1,"pair:lj4");
  memory->create(lj14_1,n+1,n+1,"pair:lj14_1");
  memory->create(lj14_2,n+1,n+1,"pair:lj14_2");
  memory->create(lj14_3,n+1,n+1,"pair:lj14_3");
  memory->create(lj14_4,n+1,n+1,"pair:lj14_4");
  memory->create(offset,n+1,n+1,"pair:offset");
}

/* ----------------------------------------------------------------------
   global settings
   unlike other pair styles,
     there are no individual pair settings that these override
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::settings(int narg, char **arg)
{
  if (narg != 2 && narg != 3) error->all(FLERR,"Illegal pair_style command");

  cut_lj_inner = utils::numeric(FLERR,arg[0],false,lmp);
  cut_lj = utils::numeric(FLERR,arg[1],false,lmp);
  if (narg == 2) cut_coul = cut_lj;
  else cut_coul = utils::numeric(FLERR,arg[2],false,lmp);
}

/* ----------------------------------------------------------------------
   set coeffs for one or more type pairs
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::coeff(int narg, char **arg)
{
  if (narg != 4 && narg != 6) error->all(FLERR,"Illegal pair_coeff command");
  if (!allocated) allocate();

  int ilo,ihi,jlo,jhi;
  utils::bounds(FLERR,arg[0],1,atom->ntypes,ilo,ihi,error);
  utils::bounds(FLERR,arg[1],1,atom->ntypes,jlo,jhi,error);

  double epsilon_one = utils::numeric(FLERR,arg[2],false,lmp);
  double sigma_one = utils::numeric(FLERR,arg[3],false,lmp);
  double eps14_one = epsilon_one;
  double sigma14_one = sigma_one;
  if (narg == 6) {
    eps14_one = utils::numeric(FLERR,arg[4],false,lmp);
    sigma14_one = utils::numeric(FLERR,arg[5],false,lmp);
  }

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo,i); j <= jhi; j++) {
      epsilon[i][j] = epsilon_one;
      sigma[i][j] = sigma_one;
      eps14[i][j] = eps14_one;
      sigma14[i][j] = sigma14_one;
      setflag[i][j] = 1;
      count++;
    }
  }

  if (count == 0) error->all(FLERR,"Incorrect args for pair coefficients");
}

/* ----------------------------------------------------------------------
   init specific to this pair style
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::init_style()
{
  if (!atom->q_flag)
    error->all(FLERR,
               "Pair style lj/charmm/coul/long requires atom attribute q");

  // request regular or rRESPA neighbor list

  int list_style = NeighConst::REQ_DEFAULT;

  if (update->whichflag == 1 && utils::strmatch(update->integrate_style, "^respa")) {
    auto respa = dynamic_cast<Respa *>( update->integrate);
    if (respa->level_inner >= 0) list_style = NeighConst::REQ_RESPA_INOUT;
    if (respa->level_middle >= 0) list_style = NeighConst::REQ_RESPA_ALL;
  }
  neighbor->add_request(this, list_style);

  // require cut_lj_inner < cut_lj

  if (cut_lj_inner >= cut_lj)
    error->all(FLERR,"Pair inner cutoff >= Pair outer cutoff");

  cut_lj_innersq = cut_lj_inner * cut_lj_inner;
  cut_ljsq = cut_lj * cut_lj;
  cut_coulsq = cut_coul * cut_coul;
  cut_bothsq = MAX(cut_ljsq,cut_coulsq);

  denom_lj = ( (cut_ljsq-cut_lj_innersq) * (cut_ljsq-cut_lj_innersq) *
               (cut_ljsq-cut_lj_innersq) );
  denom_lj_inv = 1.0 / denom_lj;

  // set & error check interior rRESPA cutoffs

  if (utils::strmatch(update->integrate_style,"^respa") &&
      (dynamic_cast<Respa *>( update->integrate))->level_inner >= 0) {
    cut_respa = (dynamic_cast<Respa *>( update->integrate))->cutoff;
    cut_in_off = cut_respa[0];
    cut_in_on = cut_respa[1];
    cut_out_on = cut_respa[2];
    cut_out_off = cut_respa[3];

    cut_in_diff = cut_in_on - cut_in_off;
    cut_out_diff = cut_out_off - cut_out_on;
    cut_in_diff_inv = 1.0 / (cut_in_diff);
    cut_out_diff_inv = 1.0 / (cut_out_diff);
    cut_in_off_sq = cut_in_off*cut_in_off;
    cut_in_on_sq = cut_in_on*cut_in_on;
    cut_out_on_sq = cut_out_on*cut_out_on;
    cut_out_off_sq = cut_out_off*cut_out_off;
    if (MIN(cut_lj,cut_coul) < cut_respa[3])
      error->all(FLERR,"Pair cutoff < Respa interior cutoff");
    if (cut_lj_inner < cut_respa[1])
      error->all(FLERR,"Pair inner cutoff < Respa interior cutoff");
  } else cut_respa = nullptr;

  // insure use of KSpace long-range solver, set g_ewald

  if (force->kspace == nullptr)
    error->all(FLERR,"Pair style requires a KSpace style");
  g_ewald = force->kspace->g_ewald;

  // setup buffers with size max neighbors per atom

  //memory->create(bufrsq,lmp->neighbor->oneatom,"pair:bufrsq");
  //memory->create(bufdelx,lmp->neighbor->oneatom,"pair:bufdelx");
  //memory->create(bufdely,lmp->neighbor->oneatom,"pair:bufdely");
  //memory->create(bufdelz,lmp->neighbor->oneatom,"pair:bufdelz");
  //memory->create(bufj,lmp->neighbor->oneatom,"pair:bufj");
  //memory->create(buftype,lmp->neighbor->oneatom,"pair:buftype");
  //memory->create(bufitable,lmp->neighbor->oneatom,"pair:bufitable");

  // setup force tables

  if (ncoultablebits) init_tables(cut_coul,cut_respa);
}

/* ----------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
------------------------------------------------------------------------- */

double PairLJCharmmCoulLong::init_one(int i, int j)
{
  if (setflag[i][j] == 0) {
    epsilon[i][j] = mix_energy(epsilon[i][i],epsilon[j][j],
                               sigma[i][i],sigma[j][j]);
    sigma[i][j] = mix_distance(sigma[i][i],sigma[j][j]);
    eps14[i][j] = mix_energy(eps14[i][i],eps14[j][j],
                               sigma14[i][i],sigma14[j][j]);
    sigma14[i][j] = mix_distance(sigma14[i][i],sigma14[j][j]);
  }

  double cut = MAX(cut_lj,cut_coul);

  lj1[i][j] = 48.0 * epsilon[i][j] * pow(sigma[i][j],12.0);
  lj2[i][j] = 24.0 * epsilon[i][j] * pow(sigma[i][j],6.0);
  lj3[i][j] = 4.0 * epsilon[i][j] * pow(sigma[i][j],12.0);
  lj4[i][j] = 4.0 * epsilon[i][j] * pow(sigma[i][j],6.0);
  lj14_1[i][j] = 48.0 * eps14[i][j] * pow(sigma14[i][j],12.0);
  lj14_2[i][j] = 24.0 * eps14[i][j] * pow(sigma14[i][j],6.0);
  lj14_3[i][j] = 4.0 * eps14[i][j] * pow(sigma14[i][j],12.0);
  lj14_4[i][j] = 4.0 * eps14[i][j] * pow(sigma14[i][j],6.0);

  lj1[j][i] = lj1[i][j];
  lj2[j][i] = lj2[i][j];
  lj3[j][i] = lj3[i][j];
  lj4[j][i] = lj4[i][j];
  lj14_1[j][i] = lj14_1[i][j];
  lj14_2[j][i] = lj14_2[i][j];
  lj14_3[j][i] = lj14_3[i][j];
  lj14_4[j][i] = lj14_4[i][j];

  return cut;
}

/* ----------------------------------------------------------------------
  proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::write_restart(FILE *fp)
{
  write_restart_settings(fp);

  int i,j;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      fwrite(&setflag[i][j],sizeof(int),1,fp);
      if (setflag[i][j]) {
        fwrite(&epsilon[i][j],sizeof(double),1,fp);
        fwrite(&sigma[i][j],sizeof(double),1,fp);
        fwrite(&eps14[i][j],sizeof(double),1,fp);
        fwrite(&sigma14[i][j],sizeof(double),1,fp);
      }
    }
}

/* ----------------------------------------------------------------------
  proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::read_restart(FILE *fp)
{
  read_restart_settings(fp);

  allocate();

  int i,j;
  int me = comm->me;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      if (me == 0) utils::sfread(FLERR,&setflag[i][j],sizeof(int),1,fp,nullptr,error);
      MPI_Bcast(&setflag[i][j],1,MPI_INT,0,world);
      if (setflag[i][j]) {
        if (me == 0) {
          utils::sfread(FLERR,&epsilon[i][j],sizeof(double),1,fp,nullptr,error);
          utils::sfread(FLERR,&sigma[i][j],sizeof(double),1,fp,nullptr,error);
          utils::sfread(FLERR,&eps14[i][j],sizeof(double),1,fp,nullptr,error);
          utils::sfread(FLERR,&sigma14[i][j],sizeof(double),1,fp,nullptr,error);
        }
        MPI_Bcast(&epsilon[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&sigma[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&eps14[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&sigma14[i][j],1,MPI_DOUBLE,0,world);
      }
    }
}

/* ----------------------------------------------------------------------
  proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::write_restart_settings(FILE *fp)
{
  fwrite(&cut_lj_inner,sizeof(double),1,fp);
  fwrite(&cut_lj,sizeof(double),1,fp);
  fwrite(&cut_coul,sizeof(double),1,fp);
  fwrite(&offset_flag,sizeof(int),1,fp);
  fwrite(&mix_flag,sizeof(int),1,fp);
  fwrite(&ncoultablebits,sizeof(int),1,fp);
  fwrite(&tabinner,sizeof(double),1,fp);
}

/* ----------------------------------------------------------------------
  proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::read_restart_settings(FILE *fp)
{
  if (comm->me == 0) {
    utils::sfread(FLERR,&cut_lj_inner,sizeof(double),1,fp,nullptr,error);
    utils::sfread(FLERR,&cut_lj,sizeof(double),1,fp,nullptr,error);
    utils::sfread(FLERR,&cut_coul,sizeof(double),1,fp,nullptr,error);
    utils::sfread(FLERR,&offset_flag,sizeof(int),1,fp,nullptr,error);
    utils::sfread(FLERR,&mix_flag,sizeof(int),1,fp,nullptr,error);
    utils::sfread(FLERR,&ncoultablebits,sizeof(int),1,fp,nullptr,error);
    utils::sfread(FLERR,&tabinner,sizeof(double),1,fp,nullptr,error);
  }
  MPI_Bcast(&cut_lj_inner,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&cut_lj,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&cut_coul,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&offset_flag,1,MPI_INT,0,world);
  MPI_Bcast(&mix_flag,1,MPI_INT,0,world);
  MPI_Bcast(&ncoultablebits,1,MPI_INT,0,world);
  MPI_Bcast(&tabinner,1,MPI_DOUBLE,0,world);
}


/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    fprintf(fp,"%d %g %g %g %g\n",
            i,epsilon[i][i],sigma[i][i],eps14[i][i],sigma14[i][i]);
}

/* ----------------------------------------------------------------------
   proc 0 writes all pairs to data file
------------------------------------------------------------------------- */

void PairLJCharmmCoulLong::write_data_all(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    for (int j = i; j <= atom->ntypes; j++)
      fprintf(fp,"%d %d %g %g %g %g\n",i,j,
              epsilon[i][j],sigma[i][j],eps14[i][j],sigma14[i][j]);
}

/* ---------------------------------------------------------------------- */

double PairLJCharmmCoulLong::single(int i, int j, int itype, int jtype,
                                    double rsq,
                                    double factor_coul, double factor_lj,
                                    double &fforce)
{
  double r2inv,r6inv,r,grij,expm2,t,erfc,prefactor;
  double switch1,switch2,fraction,table,forcecoul,forcelj,phicoul,philj;
  int itable;

  r2inv = 1.0/rsq;
  if (rsq < cut_coulsq) {
    if (!ncoultablebits || rsq <= tabinnersq) {
      r = sqrt(rsq);
      grij = g_ewald * r;
      expm2 = exp(-grij*grij);
      t = 1.0 / (1.0 + EWALD_P*grij);
      erfc = t * (A1+t*(A2+t*(A3+t*(A4+t*A5)))) * expm2;
      prefactor = force->qqrd2e * atom->q[i]*atom->q[j]/r;
      forcecoul = prefactor * (erfc + EWALD_F*grij*expm2);
      if (factor_coul < 1.0) forcecoul -= (1.0-factor_coul)*prefactor;
    } else {
      union_int_float_t rsq_lookup;
      rsq_lookup.f = rsq;
      itable = rsq_lookup.i & ncoulmask;
      itable >>= ncoulshiftbits;
      fraction = (rsq_lookup.f - rtable[itable]) * drtable[itable];
      table = ftable[itable] + fraction*dftable[itable];
      forcecoul = atom->q[i]*atom->q[j] * table;
      if (factor_coul < 1.0) {
        table = ctable[itable] + fraction*dctable[itable];
        prefactor = atom->q[i]*atom->q[j] * table;
        forcecoul -= (1.0-factor_coul)*prefactor;
      }
    }
  } else forcecoul = 0.0;
  if (rsq < cut_ljsq) {
    r6inv = r2inv*r2inv*r2inv;
    forcelj = r6inv * (lj1[itype][jtype]*r6inv - lj2[itype][jtype]);
    if (rsq > cut_lj_innersq) {
      switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
        (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
      switch2 = 12.0*rsq * (cut_ljsq-rsq) *
        (rsq-cut_lj_innersq) * denom_lj_inv;
      philj = r6inv * (lj3[itype][jtype]*r6inv - lj4[itype][jtype]);
      forcelj = forcelj*switch1 + philj*switch2;
    }
  } else forcelj = 0.0;
  fforce = (forcecoul + factor_lj*forcelj) * r2inv;

  double eng = 0.0;
  if (rsq < cut_coulsq) {
    if (!ncoultablebits || rsq <= tabinnersq)
      phicoul = prefactor*erfc;
    else {
      table = etable[itable] + fraction*detable[itable];
      phicoul = atom->q[i]*atom->q[j] * table;
    }
    if (factor_coul < 1.0) phicoul -= (1.0-factor_coul)*prefactor;
    eng += phicoul;
  }

  if (rsq < cut_ljsq) {
    philj = r6inv*(lj3[itype][jtype]*r6inv-lj4[itype][jtype]);
    if (rsq > cut_lj_innersq) {
      switch1 = (cut_ljsq-rsq) * (cut_ljsq-rsq) *
        (cut_ljsq + 2.0*rsq - 3.0*cut_lj_innersq) * denom_lj_inv;
      philj *= switch1;
    }
    eng += factor_lj*philj;
  }

  return eng;
}

/* ---------------------------------------------------------------------- */

void *PairLJCharmmCoulLong::extract(const char *str, int &dim)
{
  dim = 2;
  if (strcmp(str,"lj14_1") == 0) return (void *) lj14_1;
  if (strcmp(str,"lj14_2") == 0) return (void *) lj14_2;
  if (strcmp(str,"lj14_3") == 0) return (void *) lj14_3;
  if (strcmp(str,"lj14_4") == 0) return (void *) lj14_4;

  dim = 0;
  if (strcmp(str,"implicit") == 0) return (void *) &implicit;
  if (strcmp(str,"cut_coul") == 0) return (void *) &cut_coul;

  return nullptr;
}
