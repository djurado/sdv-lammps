#!/bin/bash

source /apps/riscv/extrae/4.0.1/etc/extrae.sh

export EXTRAE_CONFIG_FILE=./extrae.xml
#export LD_PRELOAD=${EXTRAE_HOME}/lib/libseqtrace.so # For C apps
export LD_PRELOAD=${EXTRAE_HOME}/lib/libseqtrace.so:${LD_PRELOAD} # For C apps
echo "inside trace.sh: LD_PRELOAD=${LD_PRELOAD}"
#export LD_PRELOAD=${EXTRAE_HOME}/lib/libseqtrace.so # For Fortran apps

## Run the desired program
$*

