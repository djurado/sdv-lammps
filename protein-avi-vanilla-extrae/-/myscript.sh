#!/bin/bash

cat /etc/motd | grep Release

cd "/home/djurado/riscv/perflammps/protein-avi-vanilla-extrae/-"

cp -a ../data.protein .
sed '/^run/s/.*/run 5/' ../in.protein > ./in.protein
cp -a ../data.protein .
cp -a ../extrae.xml ../trace.sh .

export PATH="/home/djurado/riscv/perflammps/lammps-avi-vanilla-extrae/install/bin:$PATH"

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development extrae/4.0.1 papi/fpga-sdv
module list

export LD_PRELOAD=/apps/riscv/papi-like/development-with-vl/lib/libpapi.so.6.0

./trace.sh lmp_avi-ser-sca -in in.protein

