#!/bin/bash

SLURM_JOB_NAME=prot-avi-vanilla-extrae-5-interactive
SLURM_JOBID=$(date +'%d_%m_%Y-%H_%M_%S')
mylogfile="${SLURM_JOB_NAME}-${SLURM_JOBID}.out"

{

rundir="/home/djurado/riscv/perflammps/protein-avi-vanilla-extrae/${SLURM_JOB_NAME}-${SLURM_JOBID}"

mkdir "${rundir}"
cd "${rundir}"
cp -a ../"${SLURM_JOB_NAME}".sh .
cp -a ../data.protein ../in.protein .
cp -a ../extrae.xml ../trace.sh .


sed -i '/^run/s/.*/run 5/' in.protein # change iterations

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development extrae/4.0.1 #papi/fpga-sdv
module list

export PATH="/home/djurado/riscv/perflammps/lammps-avi-intr1-extrae/install/bin:$PATH"

export LD_PRELOAD=/apps/riscv/extrae/4.0.1/lib/libseqtrace.so:/apps/riscv/papi-like/development-with-vl/lib/libpapi.so.6.0

ldd /home/djurado/riscv/perflammps/lammps-avi-intr1-extrae/install/bin/lmp_avi

./trace.sh lmp_avi -in in.protein

rm data.protein

} >${mylogfile} 2>&1
