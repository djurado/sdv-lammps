#!/bin/bash

myinstall="lammps-avi-vanilla-extrae"
mylogfile="log.install-${myinstall}.$(date +'%d_%m_%Y-%H_%M_%S')"

{

# Copy modified source files
# We don't want cp -a
# Cmake will only recompile if the timestamp is newer

# Create main directory
if [ -d ./${myinstall} ] ; then
  rm -rf ./${myinstall}
fi

mkdir ./${myinstall}
cd ./${myinstall}
#tar -zxf ../lammps-stable.tar.gz

cp -rv ../sources/lammps-avi-vanilla/* ../lammps-23Jun2022/src/

# Prepare shell environment
source /etc/profile.d/modules.sh
module list
module purge
module load llvm/EPI-0.7-development papi/fpga-sdv extrae/4.0.1
module list

# Create install directory
mkdir ./install
myinstalldir="$(realpath ./install)"
cmakedir="$(realpath ../lammps-23Jun2022/cmake)"

# Compilation flags
CFLAGS="-Wall -Wextra -march=rv64gc -mcpu=avispado -Ofast -ffast-math -I${EXTRAE_HOME}/include -DUSE_EXTRAE"
LFLAGS="-L${EXTRAE_HOME}/lib -lseqtrace"

# Run cmake
cmake \
  -D BUILD_MPI=no \
  -D BUILD_OMP=no \
  -D LAMMPS_MACHINE=avi \
  -D PKG_KSPACE=yes \
  -D PKG_RIGID=yes \
  -D PKG_MOLECULE=yes \
  -D PKG_OPENMP=no \
  -D FFT=KISS \
  -D FFT_SINGLE=yes \
  -D CMAKE_BUILD_TYPE=Release \
  -D CMAKE_CXX_COMPILER=clang++ \
  -D CMAKE_C_COMPILER=clang \
  -D CMAKE_CXX_FLAGS_RELEASE="${CFLAGS}" \
  -D CMAKE_C_FLAGS_RELEASE="${CFLAGS}" \
  -D CMAKE_EXE_LINKER_FLAGS="${LFLAGS}" \
  -D CMAKE_INSTALL_PREFIX="${myinstalldir}" \
  "${cmakedir}"
  
# Make with make
make VERBOSE=1 -j 4

make install
} 2>&1 | tee -a ./${mylogfile}
