#!/bin/bash
#SBATCH --job-name=prot-avi-vanilla-extrae-5
#SBATCH --output=prot-avi-vanilla-extrae-5-%j.out
#SBATCH --partition=fpga-sdv
#SBATCH --nodes=1
#SBATCH --time=0-00:30:00
##SBATCH --constraint=release:22.05

#############################################
## Construct SDV_HOST from X86_HOST        ##
#############################################
# X86_HOST: pickle-{1,2,3}
# SDV_HOST: fpga-sdv-{1,2,3}

X86_HOST="`hostname`"
SDV_HOST="fpga-sdv-`echo ${X86_HOST} | cut -d '-' -f 2`"

printf "******************************\n"
printf "* x86 node: %s\n" "${X86_HOST}"
printf "* SDV node: %s\n" "${SDV_HOST}"
printf "******************************\n\n"

rundir="/home/djurado/riscv/perflammps/protein-avi-vanilla-extrae/${SLURM_JOB_NAME}-${SLURM_JOBID}"
mkdir "${rundir}"
cd "${rundir}"
cp -a ../"${SLURM_JOB_NAME}".sh .

cat >myscript.sh <<EndOfHereDoc
#!/bin/bash

cat cat /etc/update-motd.d/99-fpga-sdv | grep -i Release
echo "I'm inside \$(hostname)"

cd "${rundir}"

cp -a ../data.protein .
sed '/^run/s/.*/run 5/' ../in.protein > ./in.protein
cp -a ../data.protein .
cp -a ../extrae.xml ../trace.sh .

export PATH="/home/djurado/riscv/perflammps/lammps-avi-vanilla-extrae/install/bin:\$PATH"

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development extrae/4.0.1 papi/fpga-sdv
module list

ldd \$(which lmp_avi)

./trace.sh lmp_avi -in in.protein

EndOfHereDoc

chmod u+x ./myscript.sh
myscript=$(realpath ./myscript.sh)

#############################################
## Commands to be executed on the SDV node ##
#############################################
# Make sure SSH keys are configured properly!
# Add "-o StrictHostKeyChecking=no" to disable key check prompt
#
# Make sure you `cd` to the working directory path!
#     You can use the $SLURM_SUBMIT_DIR variable to do so
#     You can also use a global path for <myscript>
ssh -o StrictHostKeyChecking=no ${SDV_HOST} "${myscript}"
rm data.protein

#source /etc/profile.d/modules.sh
#module load extrae/4.0.1 
#mpi2prv -f TRACE.mpits -o "${SLURM_JOB_NAME}-${SLURM_JOBID}.prv"

