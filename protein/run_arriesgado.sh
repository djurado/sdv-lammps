#!/bin/bash

# Script to run LAMMPS in arriesgado in interactive mode (without vehave)

# <this_script> <VERSION | PATH_TO_BIN> NUMITER [INPUT]
# VERSION is the name of the install directory, such as lammps-vanilla-cssr
#   Alternatively, one can simply write the path to the LAMMPS binary
# NUMITER is the number of iterations
# INPUT can be "orig" (used by default) or "mod"

# ASSUMING
# there is in.protein and data.protein in the current working directory

# To run, either
#   ./<this_script> in a shell inside arriesgado
#   use srun from hca to run in arriesgado
#   sbatch <jobscript>, which executes <this_script> inside
#
# After running, you get a .out file (can be disabled with SDVLAMMPS_NOLOG)
# and a directory with output files produced by LAMMPS and a copy of the scripts


SDVLAMMPS_MACHINE=arriesgado
SDVLAMMPS_VERSION="$1"
if [[ -f "${SDVLAMMPS_VERSION}" && -x "${SDVLAMMPS_VERSION}" ]] ; then
    SDVLAMMPS_BINARY="$(realpath ${SDVLAMMPS_VERSION})"
    SDVLAMMPS_VERSION="lmp" # used in output file
else
    SDVLAMMPS_BINARY="$(realpath ../${SDVLAMMPS_VERSION}/bin/lmp_avi)"
fi
    
SDVLAMMPS_NUMITER="$2"
SDVLAMMPS_INPUT="$3"
if [ -z ${3+x} ] ; then
  SDVLAMMPS_INPUT="orig"
fi

# select if we DON'T want to capture output to .out file,
# useful with sbatch or CI, which already capture output
# (check man bash-builtins for 0-argument invokation of exec)
if [ -z ${SDVLAMMPS_NOLOG+x} ] ; then
    SLURM_JOB_NAME="${SDVLAMMPS_VERSION}-${SDVLAMMPS_NUMITER}iter-${SDVLAMMPS_INPUT}-${SDVLAMMPS_MACHINE}"
    SLURM_JOBID=$(date +'%Y_%m_%d_%H_%M_%S')
    mylogfile="${SLURM_JOB_NAME}-${SLURM_JOBID}.out"
    exec &>${mylogfile}
fi

rundir="${SLURM_JOB_NAME}-${SLURM_JOBID}"
mkdir "${rundir}"
cd "${rundir}"

# copy sbatch jobscript (if exists)
cp -a ../"${SLURM_JOB_NAME}".sh . 2>/dev/null
# copy "run_<machine>.sh" script
cp -a ../"$0" .

# copy input
cp -a ../data.protein ../in.protein .
# change number of iterations
sed -i '/^run/s/.*/run '${SDVLAMMPS_NUMITER}'/' in.protein
# if "mod" input...
if [[ "${SDVLAMMPS_INPUT}" == "mod" ]]; then
    sed -i '/^pair_style/s/10\.0/16.1/' in.protein
fi

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development
module list

# print hostname
echo "hostname=$(hostname)"
echo "SDVLAMMPS_INPUT=${SDVLAMMPS_INPUT}"

${SDVLAMMPS_BINARY} -in in.protein

rm data.protein
