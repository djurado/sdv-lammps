#!/bin/bash

SLURM_JOB_NAME=prot-veh-intr1-cssr-5-interactive
SLURM_JOBID=$(date +'%d_%m_%Y-%H_%M_%S')
mylogfile="${SLURM_JOB_NAME}-${SLURM_JOBID}.out"

{

rundir="/home/djurado/test-ci/protein-avi-intr1-cssr/${SLURM_JOB_NAME}-${SLURM_JOBID}"

mkdir "${rundir}"
cd "${rundir}"
cp -a ../"${SLURM_JOB_NAME}".sh .
cp -a ../data.protein ../in.protein .


sed -i '/^run/s/.*/run 5/' in.protein # change iterations

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development vehave/EPI-0.7-development
module list

export PATH="/home/djurado/test-ci/lammps-avi-intr1-cssr/install/bin:$PATH"

VEHAVE_DEBUG_LEVEL=0 VEHAVE_TRACE=0 VEHAVE_VECTOR_LENGTH=16384 vehave lmp_avi -in in.protein

rm data.protein

} >${mylogfile} 2>&1
