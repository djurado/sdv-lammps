#!/bin/bash

SLURM_JOB_NAME=prot-avi-intr2-cssr-5-interactive.sh
SLURM_JOBID=$(date +'%d_%m_%Y-%H_%M_%S')
mylogfile="${SLURM_JOB_NAME}-${SLURM_JOBID}.out"

{

rundir="/home/djurado/riscv/perflammps/protein-avi-intr2-cssr/${SLURM_JOB_NAME}-${SLURM_JOBID}"

mkdir "${rundir}"
cd "${rundir}"
cp -a ../"${SLURM_JOB_NAME}".sh .
cp -a ../data.protein ../in.protein .


sed -i '/^run/s/.*/run 5/' in.protein # change iterations

source /etc/profile.d/modules.sh
module purge
module load llvm/EPI-0.7-development
module list

export PATH="/home/djurado/riscv/perflammps/lammps-avi-intr2-cssr/install/bin:$PATH"

lmp_avi -in in.protein

rm data.protein

} >${mylogfile} 2>&1
