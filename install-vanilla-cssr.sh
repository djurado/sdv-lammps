#!/bin/bash

# <./this_script> [LAMMPS_DIR] [INSTALL_DIR] [MOD_SOURCES_DIR]
#   LAMMPS_DIR is the path to the lammps-23Jun2022 directory, which cotnains src, cmake...
#   INSTALL DIR is the path whre the LAMMPS installation with "make install" will be performed
#     binary lmp_avi will be located inside INSTALL_DIR/bin
#   MOD_SOURCES_DIR is the path to the modified source files FOR THIS VERSION
# both have default value

# Build and Install the "vanilla" version of lammps
# WITH cssr read of hardware counters
#   It isn't 100% pure vanilla, since it contains modified source files to
#   read counters or for extrae instrumentation.

myinstall="lammps-vanilla-cssr"
mylogfile="log.install-${myinstall}.$(date +'%d_%m_%Y-%H_%M_%S')"

{

if [ "$#" -eq 3 ] ; then
  # get arguments + absolute path
  mylammpsdir="$(realpath $1)"
  myinstalldir="$(realpath $2)"
  mymodsourcesdir="$(realpath $3)"
else
  myinstalldir="$(realpath ${myinstall})"
  mylammpsdir="$(realpath lammps-23Jun2022)"
  mymodsourcesdir="$(realpath sources/lammps-vanilla)"
fi
mycmakedir="$(realpath ${mylammpsdir}/cmake)"

# Create and cd into install directory
if [ -d ./${myinstalldir} ] ; then
  echo Error: INSTALL_DIR already exists>&2
  #exit 1
fi
mkdir ${myinstalldir}
cd ${myinstalldir}

# Copy modified source files
# We don't want cp -a
# Cmake will only recompile if the timestamp is newer
cp -rv "${mymodsourcesdir}"/* "${mylammpsdir}"/src/


# Prepare shell environment
source /etc/profile.d/modules.sh
module list
module purge
module load llvm/EPI-0.7-development
module list

# Compilation flags
CFLAGS="-Wall -Wextra -march=rv64gc -mcpu=avispado -Ofast -ffast-math -DUSE_CSSR"


# Run cmake
cmake \
  -D BUILD_MPI=no \
  -D BUILD_OMP=no \
  -D LAMMPS_MACHINE=avi \
  -D PKG_KSPACE=yes \
  -D PKG_RIGID=yes \
  -D PKG_MOLECULE=yes \
  -D PKG_OPENMP=no \
  -D FFT=KISS \
  -D FFT_SINGLE=yes \
  -D CMAKE_BUILD_TYPE=Release \
  -D CMAKE_CXX_COMPILER=clang++ \
  -D CMAKE_C_COMPILER=clang \
  -D CMAKE_CXX_FLAGS_RELEASE="${CFLAGS}" \
  -D CMAKE_C_FLAGS_RELEASE="${CFLAGS}" \
  -D CMAKE_INSTALL_PREFIX="${myinstalldir}" \
  "${mycmakedir}"

# Make with make
make VERBOSE=1 -j 4

make install
} 2>&1 | tee -a ./${mylogfile}
